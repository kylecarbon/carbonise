## Getting started
1. Clone this repository to your preferred `setup_dir`
1. Update variables in `<setup_dir>/ansible/carbons_playbook.yaml`
  1. ensure `setup_dir` points to where you cloned this repository (eg. `~/carbonise`)
  1. set `dest_dir` for where to checkout repositories when building from source (eg. `~/opt`)
1. Run setup script:

```
cd <setup_dir>/ansible
./carbonise
```

## Details
##### carbonise/ansible
This directory contains my ansible playbook to setup my workspace. In addition to the individual roles, I also have tags for
the following:

> **Note**: only the `common` tag installs programs without any additional configuration. All other tags will alter your system in at least the following locations: `~/.config`, `dconf`.

1. `common`: installs things that _most_ people should find useful.
  1. Programs: google chrome, firefox, neovim, slack, google play music, silversearcher-ag, various applets, etc.
  1. Fonts: [nerd-fonts](https://github.com/ryanoasis/nerd-fonts), [powerline-fonts](https://github.com/powerline/fonts),
        [FontAwesome](http://fontawesome.io/)
      1. nerd-fonts should be enough for most people, as they've patched several popular fonts (including a couple with 
       ligature support) to include both Powerline and FontAwesome icons. However, I have personally found occassions
       where I want neither ligatures nor icons, so I default to Powerline fonts in those scenarios.
      1. The FontAwesome role downloads a tarball and extracts it. I got annoyed with it running every instance, so I
       commented it out. It's on my TODO list to install it through npm
  1. Themes: arc, paper, papirus
1. `workspace`: installs things that _some_ people will find useful.
  1. Programs: tilix, neovim, SublimeText3, julia
1. `desktop`: sets up my desktop exactly the way I like it. Checkout some screenshots below, as well as [/r/unixporn](www.reddit.com/r/unixporn).
  1. Programs: i3, i3lock-(color, fancy, etc.), polybar, rofi, powerline, neofetch

##### carbonise/dotfiles
`dotfiles` contains my dotfiles for `bash`, `git`, `i3`, etc. My ansible playbook uses these dotfiles when setting
up my custom configurations.
##### carbonise/Pictures
This directory contains pictures that I commonly use, eg. profile picture, avatar, neofetch, etc.

#### Images
![Figure: screenshot of desktop with SublimeText3, tilix, and neofetch.][screenshot_desktop]
![Figure: screenshot of locked screen with cloud-lock.][screenshot_locked_screen]

[screenshot_desktop]: Pictures/screenshots/desktop.png "SublimeText3, tilix, neofetch."
[screenshot_locked_screen]: Pictures/screenshots/locked_screen.png "Custom lock screen with cloud-lock"

#### TODO
* [ ] insert block into bottom of ssh config
* [ ] install font-awesome through `npm`
* [ ] watch for `julia v1.0` soon
* [ ] decouple installation from configuration