#!/bin/bash
############################
# install.sh
# This script installs custom cron jobs
############################

########## Variables
src_dir=~/dotfiles/cron           # dotfiles directory
old_dir=~/dotfiles_old/cron    	  # old dotfiles backup directory
file="my_cron"    			 	  # list of file/folders to symlink in homedir
##########

echo "===== Configuring cron jobs ====="
# create dotfile_old in homedir
echo " - Creating $old_dir for backup of any existing cron jobs"
mkdir -p $old_dir
cd $old_dir
crontab -l > $file

# change to the source directory
echo " - Changing directory to: $src_dir"
cd $src_dir
echo " - Creating new cron job from $src_dir/$file"
crontab $file

echo "Done!"
