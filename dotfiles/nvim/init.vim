""""""""""""""""""""""""""""""""""""""""""""""""
" Neovim config
" Author: Kyle Carbon (kylecarbon@gmail.com)
""""""""""""""""""""""""""""""""""""""""""""""""
let s:dein_dir = expand('/opt/dein')
let s:dein_repo_dir = s:dein_dir . '/repos/github.com/Shougo/dein.vim'

if &compatible
  set nocompatible
endif

if &runtimepath !~# '/dein.vim'
  if !isdirectory(s:dein_repo_dir)
    execute '!git clone https://github.com/Shougo/dein.vim' s:dein_repo_dir
  endif
  execute 'set runtimepath^=' . fnamemodify(s:dein_repo_dir, ':p')
endif

if dein#load_state(s:dein_dir)
  call dein#begin(s:dein_dir)

  call dein#add(s:dein_repo_dir)
  call dein#add('Shougo/deoplete.nvim')
  call dein#add('Shougo/deoplete-clangx')
  call dein#add('Shougo/neoinclude.vim')
  call dein#add('zchee/deoplete-jedi')
  call dein#add('Shougo/denite.nvim')
  call dein#add('scrooloose/nerdtree')
  call dein#add('scrooloose/nerdcommenter')
  call dein#add('airblade/vim-gitgutter')
  call dein#add('arcticicestudio/nord-vim')
  call dein#add('tpope/vim-fugitive')
  call dein#add('vim-airline/vim-airline')
  call dein#add('vim-airline/vim-airline-themes')
  call dein#add('edkolev/promptline.vim')
  call dein#add('cespare/vim-toml')
  call dein#add('majutsushi/tagbar')
  call dein#add('easymotion/vim-easymotion')
  call dein#add('w0rp/ale')
  call dein#add('sheerun/vim-polyglot')
  call dein#add('haya14busa/incsearch.vim')
  call dein#add('jacoborus/tender.vim')
  call dein#add('kristijanhusak/vim-hybrid-material')
"  call dein#add('daeyun/vim-matlab')
  call dein#add('zhou13/vim-easyescape')
  call dein#add('JuliaEditorSupport/julia-vim')

  call dein#end()
  call dein#save_state()
endif

if dein#check_install()
  call dein#install()
endif

filetype plugin indent on
syntax enable

"""""""""""" custom settings """"""""""""
set number                      " line numbering on
set relativenumber              " relative line numbering (if number is on, this enables hybrid numbering)
augroup numbertoggle
  autocmd!
  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
augroup END

set noswapfile                  " no swap file!
set expandtab
set tabstop=2
set shiftwidth=2
set wildmenu
set wildmode=longest:full,full
set wildignore+=*.swp,*.bak,*.dll,*.o,*.obj,*.pyc.*.exe
set wildignore+=*.jpg,*.gif,*.png,*.class,*.ln
" set listchars=tab:>-,trail:-,eol:$,nbsp:%
set colorcolumn=120

" Setting <leader> in case it is changed
let mapleader="\\"              " Default <leader> key
"This allows 'showcmd' to work with <space>
map <Space> <leader>
let maplocalleader=","          " Default <localleader> key
let g:easyescape_chars = { "j": 2}
let g:easyescape_timeout = 100
inoremap jj <ESC>

" Shortcut to edit/source vimrc file
nnoremap <leader>ev :e! $MYVIMRC<cr>
nnoremap <leader>sv :source $MYVIMRC<cr>

" Use tab to cycle through popup menus
inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
map <leader>n :bn<cr>
map <leader>d :bp<cr>

" Searching options
set ignorecase
set smartcase
set infercase
set incsearch
set list
set hlsearch
nnoremap <silent> <F6> :set hlsearch!<CR>
map /    <Plug>(incsearch-forward)
map ?    <Plug>(incsearch-backward)
map g/   <Plug>(incsearch-stay)
let g:incsearch#auto_nohlsearch = 1
map n  <Plug>(incsearch-nohl-n)
map N  <Plug>(incsearch-nohl-N)
map *  <Plug>(incsearch-nohl-*)
map #  <Plug>(incsearch-nohl-#)
map g* <Plug>(incsearch-nohl-g*)
map g# <Plug>(incsearch-nohl-g#)

"""""""""""" Denite settings """"""""""""
" Change file_rec command.
call denite#custom#var('file_rec', 'command', ['ag', '--follow', '--nocolor', '--nogroup', '-g', ''])
" Ag command on grep source
call denite#custom#var('grep', 'command', ['ag'])
call denite#custom#var('grep', 'default_opts', ['-i', '--vimgrep'])
call denite#custom#var('grep', 'recursive_opts', [])
call denite#custom#var('grep', 'pattern_opt', [])
call denite#custom#var('grep', 'separator', ['--'])
call denite#custom#var('grep', 'final_opts', [])
nnoremap <leader>p :<C-u>Denite file_rec<CR>
call denite#custom#map('insert', '<C-j>', '<denite:move_to_next_line>', 'noremap')
call denite#custom#map('insert', '<C-k>', '<denite:move_to_previous_line>', 'noremap')
call denite#custom#option('default', 'highlight_matched_char', 'PreProc')
nnoremap <leader>8 :<C-u>DeniteCursorWord grep:. -mode=normal<CR>
nnoremap <silent> <C-f> :<C-u>Denite -buffer-name=search -auto-highlight line<CR>

"""""""""""" Filetype handling """"""""""""
au BufRead,BufNewFile *.{markdown,mdown,mkd,mkdn,md} set syntax=markdown
au BufRead,BufNewFile *.{launch} set syntax=xml
au BufRead,BufNewFile *.{jade} set syntax=jade
au BufRead,BufNewFile *.{rs} set syntax=rust filetype=rust
au BufRead,BufNewFile *.{s,Makefile} set noet sw=8 ts=8
au BufRead,BufNewFile *.{c,cpp,h,hpp} set et sw=2 ts=2
au BufRead,BufNewFile *.{java,ml,scala} set et sw=4 ts=4
au BufRead,BufNewFile *.{conf} set et sw=2 ts=2
au BufRead,BufNewFile *.{jl} set et sw=4 ts=4
augroup filetype_go
  au FileType go set noet sw=4 ts=4 sts=4
augroup END
augroup filetype_ruby
  au FileType ruby set et sw=2 ts=2
augroup END
augroup filetype_html
  au FileType html set et sw=4 ts=4
augroup END
augroup filetype_python
  au FileType python set et sw=4 ts=4 cc=100
augroup END
augroup filetype_shell
  au FileType sh set et sw=2 ts=2
augroup END

"""""""""""" Use deoplete """"""""""""
let g:deoplete#enable_at_startup=1
"let g:deoplete#sources#clang#executable='/usr/bin/clang'
"let g:deoplete#sources#clang#flags = ['c', 'cpp', 'cmake']
"let g:deoplete#sources#clang#libclang_path = ''
"let g:clang_library_path='usr/lib/llvm-3.8/lib'

"""""""""""" nord settings """"""""""""
let g:nord_italic = 1
let g:nord_uniform_status_lines = 1
set termguicolors
let g:nord_uniform_diff_background = 1
colorscheme nord
highlight Normal guibg=none
highlight NonText guibg=none

"""""""""""" airline-settings """"""""""""
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#formatter = "unique_tail_improved"
let g:airline_powerline_fonts = 1
let g:airline_theme = "nord"
let g:airline_detect_modified=1
let g:airline_detect_paste=1
let g:airline#extensions#tabline#showbuffers=1

"""""""""""" NERDTree Settings """"""""""""
noremap <C-n> :NERDTreeToggle<CR>
let g:NERDTreeDirArrowExpandable = '▸'
let g:NERDTreeDirArrowCollapsible = '▾'

"""""""""""" clang-format """"""""""""
nnoremap <C-K> :py3f ~/opt/clang-format/clang-format.py<cr>
inoremap <C-K> <c-o>:py3f ~/opt/clang-format/clang-format.py<cr>
noremap <C-a> <esc>gg0VG$<CR>

"""""""""""" vim-matlab """"""""""""""
"let g:matlab_server_launcher = 'nvim'

"""""""""""" vim-matlab """"""""""""""
"let g:latex_to_unicode_auto = 1

"""""""""""" NERDCommenter """"""""""""
nmap <C-_>   <Plug>NERDCommenterToggle
vmap <C-_>   <Plug>NERDCommenterToggle<CR>gv
