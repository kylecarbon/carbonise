personal_library = ['/home/' getenv('USER') '/Documents/MATLAB/library'];
addpath(personal_library)

% set default window style
set(0,'DefaultFigureWindowStyle','docked')

% % set default precision to 10 sig-figs in data cursor for plots
% set(0,'defaultFigureCreateFcn',@(s,e)data_cursor_precision(s));
% dcmObj = datacursormode;
% set(dcmObj, 'UpdateFcn', @data_cursor_precision)

% set default fonts
% set(0, 'defaultFontName', 'Ubuntu')
set(0, 'defaultAxesFontName', 'Ubuntu')
set(0, 'defaultTextFontName', 'Ubuntu')
set(0, 'defaultLegendFontName', 'Ubuntu')
% set(0, 'defaultTitleFontName', 'Ubuntu')
set(0,'defaultfigurecolor',[1 1 1])

pyversion('/usr/bin/python3')
