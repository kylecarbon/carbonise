function y = my_filt_fir(b, x)

N = length(x);
window_size = length(b);
b = fliplr(b(:)');
y = zeros(N, 1);
x_subset = zeros(window_size, 1);
for i = 1:N
    ind_end = i;
    ind_start = i - window_size + 1;
    if ind_start < 1
        ind_start = 1;
        x_subset(ind_start:ind_end) = x(ind_start:ind_end);
    else
        x_subset(:) = x(ind_start:ind_end);
    end
    
    y(i) = b*x_subset;
    
end






