function angle = my_angle(X)

angle = atan2( imag(X), real(X) );


end