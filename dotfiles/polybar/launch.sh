#!/bin/bash
# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# Find all monitors
ALL_MONITORS=$(xrandr | grep " connected" | awk '{ print $1 }' )

# Launch my_bar
for m in $ALL_MONITORS; do
    if [ "$HOSTNAME" == "cloud" ]; then
        MONITOR=$m polybar my_bar &
    else
        MONITOR=$m polybar my_bar &
    fi
done

echo "Bars launched..."
